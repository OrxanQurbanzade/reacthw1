import React, {Component} from 'react';
import Button from "./Button";
import "./ProductCart.scss"
import { MdStar} from "react-icons/all";
class ProductCard extends Component{
    state={
        starColor :"black"
    };

    clickHandler = () => {
        this.props.addToFavorites(this.props.productNumber);
        this.setState({starColor :"gold"})

    };

    render() {
        return (
            <div className={"card"}>
                {/* eslint-disable-next-line jsx-a11y/img-redundant-alt */}
                <img src={this.props.productImg} alt ="image" className={"card-img"}/>
                <p className={"card-name"}>{this.props.productName}</p>

                <div className={"card-price-button"}>
                    <span className={"card-price"}>{this.props.productPrice}</span>
                    <span className={"card-star"} onClick={this.clickHandler}>
                        <MdStar
                            color={this.state.starColor}
                            size="2em"
                            style={{ padding: "2px"}}
                        />
                    </span>
                    <Button text={"Add To Cart"} clicked={() =>{ this.props.addToCart(this.props.productNumber)}} />
                </div>

            </div>
        );
    }


}

export default ProductCard;
