import React, {Component} from 'react';
import ProductCard from "./ProductCard";
import "./ProductList.scss"

class ProductList extends Component {
    render() {
        return (
            <div className={"product-list-wrapper"}>
                <div className={"product-list"}>
                    {this.props.products.map((product) => {
                        return <ProductCard key={product.number} productName={product.name} productPrice={product.price} productImg={product.path} addToCart={this.props.addToCart} addToFavorites={this.props.addToFavorites} productNumber={product.number}
                        />
                    })}
                </div>
            </div>

        );
    }
}

export default ProductList;
