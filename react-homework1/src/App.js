import React, {Component} from 'react';
import ModalWindow from "./Components/ModalWindow";
import Button from "./Components/Button";
class App extends Component {
  state = {
    firstModal: false,
    secondModal: false,
  };

  showFirstModal = () => {
    this.setState({
      firstModal:!this.state.firstModal,
    });
  };

  showSecondModal = (e) => {
    this.setState({
      secondModal:!this.state.secondModal,
    });
  };

  render() {
    return (
        <div className="App">
          <Button text={"Delete"} color={"#1b17b3"} clicked={this.showFirstModal}/>
          <Button text={"Quit"} color={"#b3100e"} clicked={this.showSecondModal}/>
          {
            this.state.firstModal ?
                <ModalWindow closed={this.showFirstModal}
                             windowtext={"Once you delete this file, it won't be possible to undo this action.Are you sure you want to delete it?"}
                             headertext={"Do you want to delete this file?"}
                             action={[<Button text={"Ok"} color={"#b3382c"}/>,<Button text={"Cancel"} color={"#b3382c"}/>]}
                /> : null
          }
          {
            this.state.secondModal ?
                <ModalWindow closed={this.showSecondModal}
                             windowtext={"Once you exit this file, changes will not be saved."}
                             headertext={"Do you want to Save this file?"}
                             action={[<Button text={"Save"} color={"#4e8eb3"}/>,<Button text={"Don't Save"} color={"#4170b3"}/>]}
                /> : null
          }
        </div>
    );
  }
}
export default App;
